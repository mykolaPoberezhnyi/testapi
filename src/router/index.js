import Vue from 'vue'
import Router from 'vue-router'

import Posts from '../pages/Posts'
import Create from '../pages/Create'
import Change from '../pages/Change'

Vue.use(Router)

let router = new Router({
    routes: [
        {
            path: "/posts",
            name: "Posts",
            component: Posts
        },
        {
            path: "/change",
            name: "Change",
            component: Change
        },
        {
            path: "/create",
            name: "Create",
            component: Create
        }
    ]
})

export default router