import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Axios from 'axios'
 
Vue.use(Vuetify)
Vue.use(Axios)

import router from './router'

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
